#include <iostream>
#include <unicode/coll.h>
#include <unicode/ures.h>

#include <QStringList>
#include <QDebug>


using namespace std;

icu::UnicodeString qStringToUnicodeString(const QString &sourceStr)
{
    return UnicodeString(static_cast<const UChar *>(sourceStr.utf16()),
                   sourceStr.length());
}


int main()
{
    // language list obtained from translate.sailfishos.org
    QStringList localesToTest;
    localesToTest << "hu_HU";
    localesToTest << "en_US";
    localesToTest << "en_GB";
    localesToTest << "be_tarask";
    localesToTest << "bn_IN";
    localesToTest << "nb";
    localesToTest << "bg";
    localesToTest << "ca";
    localesToTest << "zh_TW";
    localesToTest << "cs";
    localesToTest << "da";
    localesToTest << "nl";
    localesToTest << "eo";
    localesToTest << "et";
    localesToTest << "fi";
    localesToTest << "fr";
    localesToTest << "gl";
    localesToTest << "el";
    localesToTest << "gu";
    localesToTest << "hi";
    localesToTest << "id";
    localesToTest << "en_GA";
    localesToTest << "ja";
    localesToTest << "kn";
    localesToTest << "zh_HK";
    localesToTest << "zh_CN";
    localesToTest << "ko";
    localesToTest << "lv";
    localesToTest << "pl";
    localesToTest << "lt";
    localesToTest << "ml";
    localesToTest << "mr";
    localesToTest << "de";
    localesToTest << "oc";
    localesToTest << "it";
    localesToTest << "ru";
    //localesToTest << "pa"; // TODO figure out how to use Panjabi
    localesToTest << "pt";
    localesToTest << "pt_BR";
    localesToTest << "ro";
    localesToTest << "sr";
    localesToTest << "sk";
    localesToTest << "sl";
    localesToTest << "es";
    localesToTest << "es_ES";
    localesToTest << "sv";
    localesToTest << "ta";
    localesToTest << "tt";
    localesToTest << "te";
    localesToTest << "th";
    localesToTest << "tr";
    localesToTest << "uk";
    localesToTest << "cy";

    UErrorCode status = U_ZERO_ERROR;
    icu::Collator *collator = icu::Collator::createInstance(status);
    if (U_FAILURE(status)) {
        cout << "Error" << status;
        return -1;
    }

    foreach (QString locale, localesToTest) {
        const char *collationLocaleName = locale.toLocal8Bit().constData();
        status = U_ZERO_ERROR;
        UResourceBundle *res = ures_open(NULL, collationLocaleName, &status);

        if (U_FAILURE(status)) {
            cout << "Error ures_open" << collationLocaleName << u_errorName(status);
            ures_close(res);
            return 0;
        }

        int32_t len;
        status = U_ZERO_ERROR;
        QString charStr;
        const UChar *val = ures_getStringByKey(res, "ExemplarCharactersIndex", &len, &status);
        if (U_FAILURE(status)) {
            qDebug() << locale << "Error ures_getStringByKey" << collationLocaleName << u_errorName(status);
            ures_close(res);
        } else {
            charStr = QString::fromUtf16(val, len);
            charStr.remove('[');
            charStr.remove(']');
            charStr.remove('{');
            charStr.remove('}');
            ures_close(res);
        }

        charStr = charStr.toUpper();
        /*charStr.append(' ');
        charStr.append(charStr.toLower());*/
        QStringList letters = charStr.split(' ', QString::SkipEmptyParts);
        foreach (QString a, letters) {
            foreach (QString b, letters) {
                collator->setStrength(icu::Collator::PRIMARY);
                UCollationResult resultSecondary =
                        (UCollationResult)collator->compare(
                            qStringToUnicodeString(a),
                            qStringToUnicodeString(b));
                collator->setStrength(icu::Collator::SECONDARY);
                UCollationResult resultQuaternary =
                        (UCollationResult)collator->compare(
                            qStringToUnicodeString(a),
                            qStringToUnicodeString(b));
                if (resultQuaternary != resultSecondary) {
                    qWarning() << "Mismatch when comparing " << a << b << " with locale" << locale;
                    //qWarning() << "Secondary  :" << resultSecondary;
                    //qWarning() << "Quaternary :" << resultQuaternary;
                }
            } // foreach on letters inner
        }// foreach on letters outer
    }
    return 0;
}
