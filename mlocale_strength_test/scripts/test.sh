 #!/bin/bash
 
 declare -a arr=("bn_IN"
"cs_CZ"
"da_DK"
"de_DE"
"el_GR"
"en_GB"
"en_US"
"es_ES"
"es_BO"
"et_EE"
"fi_FI"
"fr_FR"
"gu_IN"
"hi_IN"
"hu_HU"
"it_IT"
"kn_IN"
"ml_IN"
"mr_IN"
"nb_NO"
"nl_NL"
"pa_IN"
"pl_PL"
"pt_PT"
"pt_BR"
"ru_RU"
"sl_SI"
"sv_SE"
"ta_IN"
"te_IN"
"tr_TR"
"tt_RU"
"zh_CN"
"zh_HK"
"zh_TW"
"ja_JP"
)


## now loop through the above array
for i in "${arr[@]}"
do
    LANG=$i.utf8 /usr/bin/mlocale_strength_test $i
done
