/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>

/*
  Copyright (C) 2013 Jolla Ltd.
  Contact: Thomas Perl <thomas.perl@jollamobile.com>
  All rights reserved.

  You may use this file under the terms of BSD license as follows:

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Jolla Ltd nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR
  ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

#include <QDebug>

#include <mlocale5/mlocale.h>
#include <mlocale5/mcollator.h>

using namespace ML10N;
MLocale mLocale;

QString indexBucketSecondary(const QString &str)
{
    QStringList bucketList = mLocale.exemplarCharactersIndex();
    MCollator coll = mLocale.collator();
    coll.setStrength(MLocale::CollatorStrengthSecondary);
    return mLocale.indexBucket(str, bucketList, coll);
}

int main(int argc, char *argv[])
{
    if (argc != 2) {
        qWarning() << "Usage: mlocale_strength_test LANGCODE";
        return -1;
    }
    qWarning() << "LANG: " << qgetenv("LANG");

    // language list obtained from translate.sailfishos.org
    QStringList localesToTest;
    localesToTest << "hu_HU";
    localesToTest << "en_US";
    localesToTest << "en_GB";
    localesToTest << "be_tarask";
    localesToTest << "bn_IN";
    localesToTest << "nb_NO";
    localesToTest << "bg";
    localesToTest << "ca";
    localesToTest << "zh_TW";
    localesToTest << "cs";
    localesToTest << "da";
    localesToTest << "nl";
    localesToTest << "eo";
    localesToTest << "et";
    localesToTest << "fi";
    localesToTest << "fr";
    localesToTest << "gl";
    localesToTest << "el";
    localesToTest << "gu";
    localesToTest << "hi";
    localesToTest << "id";
    localesToTest << "en_GA";
    localesToTest << "ja";
    localesToTest << "kn";
    localesToTest << "zh_HK";
    localesToTest << "zh_CN";
    localesToTest << "ko";
    localesToTest << "lv";
    localesToTest << "pl";
    localesToTest << "lt";
    localesToTest << "ml";
    localesToTest << "mr";
    localesToTest << "de";
    localesToTest << "oc";
    localesToTest << "it";
    localesToTest << "ru";
    //localesToTest << "pa_IN";
    localesToTest << "pt";
    localesToTest << "pt_BR";
    localesToTest << "ro";
    localesToTest << "sr";
    localesToTest << "sk";
    localesToTest << "sl";
    localesToTest << "es";
    localesToTest << "es_ES";
    localesToTest << "sv";
    localesToTest << "ta";
    localesToTest << "tt";
    localesToTest << "te";
    localesToTest << "th";
    localesToTest << "tr";
    localesToTest << "uk";
    localesToTest << "cy";

    //foreach (QString locale, localesToTest) {
        QString locale = QString(argv[1]);
        mLocale.setCategoryLocale(MLocale::MLcCollate, locale);
        QStringList letters = mLocale.exemplarCharactersIndex();
        foreach (QString letter, letters) {
            QString a = mLocale.indexBucket(letter);
            QString b = indexBucketSecondary(letter);
            if (a != b) {
                qWarning() << "Bucket mismatch for " << letter <<" with locale" << locale << a << " with primary" << b << " with secondary";
            } else {
                //qWarning() << "Bucket matched" << letter <<" with locale" << locale << a << " with primary" << b << " with secondary";
            }
        }// foreach on letters
    //}
    return 0;
}
